package existence;

import coupling.Experiment;
import coupling.Result;
import coupling.interaction.Interaction020;
import existence.Existence010.Mood;

/**
 * An Existence02010 is a mix of Existence 20 and Existence010 in which the agent is looking for positive
 * valence interactions. When an interaction is correctly guessed 3 times in a row, the agent gets bored and  
 * changes the experiment.  
 */

public class Existence2010 extends Existence020 {

	@Override
	protected void initExistence(){
		Experiment e1 = addOrGetExperience(LABEL_E1);//Creation of an experiment in the environment 
		Experiment e2 = addOrGetExperience(LABEL_E2);
		Result r1 = createOrGetResult(LABEL_R1);//creation of a result in the environment
		Result r2 = createOrGetResult(LABEL_R2);

		/** Change the valence of interactions to change the agent's motivation */
		 //the following lines are commented as they create interaction known by the agent.
		// As we don't want the agent to know all the results from the beginning, we comment the following lines
		// the agent will learn by himself.
	//	addOrGetPrimitiveInteraction(e1, r1, -1); 
	//	addOrGetPrimitiveInteraction(e1, r2, 1);
	//	addOrGetPrimitiveInteraction(e2, r1, -1);
	//	addOrGetPrimitiveInteraction(e2, r2, 1);
		this.setPreviousExperience(e1);
		}
	
	@Override
	public String step() {
		
		Experiment experience = this.getPreviousExperience();
		if (this.getMood() == Mood.PAINED || this.getMood() == Mood.BORED)
			experience = getOtherExperience(experience); //if the agent is pained or bored, we change the experiment
		
		if (this.getMood() == Mood.BORED)
			this.setSelfSatisfactionCounter(0);
		
		Result anticipatedResult = predict(experience); //the agent predicts a result
		Result result = returnResult010(experience); //the experiment is done for real
		int valence = returnValence(experience); //the valence of the experiment is attributed
		
		//The interaction is created and added to the agent memory
		Interaction020 enactedInteraction = (Interaction020)this.addOrGetPrimitiveInteraction(experience, result, valence);
		
		if (enactedInteraction.getValence() >= 0){
			if (result == anticipatedResult){
				this.setMood(Mood.SELF_SATISFIED);
				this.incSelfSatisfactionCounter();
			}
			else{
				this.setMood(Mood.FRUSTRATED);
				this.setSelfSatisfactionCounter(0);
			}
		}
		else
			this.setMood(Mood.PAINED);
		
		if (this.getSelfSatisfactionCounter() >= BOREDOME_LEVEL)
			this.setMood(Mood.BORED);
		
		this.setPreviousExperience(experience);
		
		return experience.getLabel() + result.getLabel() + " " + this.getMood();
	}
	

	/**
	 * The Environment2010
	 * E1 results in R1, valence is -1; E2 results in R2, valence is 1.
	 * @param experience: The current experience.
	 * @return The result of this experience.
	 */
	public int returnValence(Experiment experience){
		if (experience.equals(addOrGetExperience(LABEL_E1)))
			return -1;//valence of experiment 1 is -1.
		else
			return 1;//valence of experiment 2 is 1.
	}
}
